# QA

https://contrast-finder.fr

## URL
* [http  + with www](http://www.contrast-finder.fr)
* [http  + without www](http://contrast-finder.fr)
* [https + with www](https://www.contrast-finder.fr)
* [https + without www](https://contrast-finder.fr)

### Files

* [/humans.txt](https://contrast-finder.fr/humans.txt)
* [/robots.txt](https://contrast-finder.frh/robots.txt)
* [/.well-known/security.txt](https://contrast-finder.fr/.well-known/security.txt)

## QA - Online tools
`*` preconfigured tools


* Security 
    * [Hardenize](https://www.hardenize.com) (DNS, SMTP, web server)
    * [Mozilla Observatory](https://observatory.mozilla.org/analyze/contrast-finder.fr) `*` (HTTP header, SSL, cookies, ...)
    * [Security Headers](https://securityheaders.io/?q=https://contrast-finder.fr) `*` (HTTP header)
    * Content-Security-Policy (CSP)
        * [cspvalidator.org](https://cspvalidator.org/#url=https://contrast-finder.fr) `*` 
        * [csp-evaluator.withgoogle.com](https://csp-evaluator.withgoogle.com/?csp=https://contrast-finder.fr) `*` 
    * SSL
        * [ssllabs.com](https://www.ssllabs.com/ssltest/analyze?d=contrast-finder.fr) `*` 
        * [tls.imirhil.fr](https://tls.imirhil.fr/https/contrast-finder.fr) `*` 
    * DNS
        * [DNSViz](http://dnsviz.net/d/contrast-finder.fr/dnssec/) `*` (DNSSEC)
        * [DNSSEC Analyzer (Verisign Labs)](https://dnssec-debugger.verisignlabs.com/contrast-finder.fr) `*`   (DNSSEC)
        * [Zonemaster (iiS and AFNIC)](https://zonemaster.net/domain_check)
* W3C tools
    * [HTML validator](https://validator.w3.org/nu/?doc=https://contrast-finder.fr&showsource=yes&showoutline=yes&showimagereport=yes) `*` 
    * [CSS validator](https://jigsaw.w3.org/css-validator/validator?uri=https://contrast-finder.fr&profile=css3) `*` 
    * [i18n checker](https://validator.w3.org/i18n-checker/check?uri=https://contrast-finder.fr) `*` 
    * [Link checker](https://validator.w3.org/checklink?uri=https://contrast-finder.fr&hide_type=all&depth=&check=Check) `*` 
* Web accessibility
    * [Asqatasun](https://app.asqatasun.org)
* Web perf
    * [Yellowlab](http://yellowlab.tools)
    * [Webpagetest](https://www.webpagetest.org/)
    * [Test a single asset from 14 locations](https://tools.keycdn.com/performance?url=https://contrast-finder.fr) `*`
* HTTP/2
    * [Http2.pro](https://http2.pro/check?url=https://contrast-finder.fr) `*` (check server HTTP/2, ALPN, and Server-push support)
* Global tools (webperf, accessibility, security, ...)
    * [Dareboost](https://www.dareboost.com)  (free trial)
    * [Sonarwhal](https://sonarwhal.com/scanner/)

* Social networks
  * [Twitter card validator](https://cards-dev.twitter.com/validator)
* structured data (JSON-LD, rdf, schema.org, microformats.org, ...)
  * [Google structured data testing tool](https://search.google.com/structured-data/testing-tool#url=https://contrast-finder.fr/)  `*` 
  * [Yandex structured data testing tool](https://webmaster.yandex.com/tools/microtest/)
  * [Structured Data Linter](http://linter.structured-data.org/?url=https://contrast-finder.fr)  `*` 
  * [Microdata Parser](http://tools.seomoves.org/microdata/)
* Google image
  * [images used on the website](https://www.google.fr/search?tbm=isch&q=site:contrast-finder.fr)  `*`  (site:contrast-finder.fr)
  * [images used on the website but hosted on other domains](https://www.google.fr/search?tbm=isch&q=site:contrast-finder.fr+-src:contrast-finder.fr) `*`  (site:contrast-finder.fr -src:contrast-finder.fr)
  * [images hosted on the domain name](https://www.google.fr/search?tbm=isch&q=src:contrast-finder.fr)  `*`    (src:contrast-finder.fr)
  * [images hosted on the domain name and used by other domain names (hotlinks)](https://www.google.fr/search?tbm=isch&q=src:contrast-finder.fr+-site:contrast-finder.fr)  `*`   (src:contrast-finder.fr -site:contrast-finder.fr)


