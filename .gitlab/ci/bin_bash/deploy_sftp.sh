#!/bin/bash

#######################################################################
set -o errexit  # exit script when command fails
set -o nounset  # exit script when it tries to use undeclared variables
set -o pipefail # returns error from pipe `|` 
                # if any of the commands in the pipe fail 
                # (normally just returns an error if the last fails)
#######################################################################

# Config FTP
# ----> Go to GitLab > Project > Settings > CI/CD Pipelines > Secret Variables
FTP_HOST=${PRIVATE_FTP_HOST}
FTP_USERNAME=${PRIVATE_FTP_USERNAME}
FTP_PASSWORD=${PRIVATE_FTP_PASSWORD}

# Path (local + remote)
LOCAL_PATH="OVH_start"
REMOTE_PATH="/"

#######################################################################
LFTP_OPEN="open ${FTP_HOST}; user ${FTP_USERNAME} ${FTP_PASSWORD};"
LFTP_OPT=""
LFTP_OPT="${LFTP_OPT} mirror"
LFTP_OPT="${LFTP_OPT} --reverse"
LFTP_OPT="${LFTP_OPT} --verbose=3"
LFTP_OPT="${LFTP_OPT} --delete"
LFTP_OPT="${LFTP_OPT} -X .git*"
LFTP_CMD="${LFTP_OPEN} ${LFTP_OPT} ${LOCAL_PATH} ${REMOTE_PATH}; bye"

    echo "--------------"
    echo "${LOCAL_PATH} ----> ${REMOTE_PATH}"
    echo "--------------"
    echo "${FTP_HOST}" 
    echo "--------------"
    echo "lftp -e \"${LFTP_CMD}\""
    echo "--------------"

# Start Update
lftp -e "${LFTP_CMD}"
    # documentation https://lftp.tech/lftp-man.html



