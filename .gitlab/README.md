# .gitlab

## .gitlab/issue_templates

documentation:
- https://docs.gitlab.com/ce/user/project/description_templates.html

examples:
- https://gitlab.com/gitlab-org/gitlab-ce/tree/master/.gitlab/issue_templates

## .gitlab/merge_request_templates
examples:
- https://gitlab.com/gitlab-org/gitlab-ce/tree/master/.gitlab/merge_request_templates

## .gitlab/ci

documentation:
- https://docs.gitlab.com/ce/ci/yaml/#include

examples:
- https://gitlab.com/gitlab-org/gitlab-ce/tree/master/.gitlab/ci
- https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.gitlab-ci.yml

